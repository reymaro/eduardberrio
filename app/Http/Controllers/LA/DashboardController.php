<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use DB;
/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $role = DB::table('role_user')->select('role_id')->where('user_id',Auth::user()->id)->get();
        if ($role[0]->role_id != '1') {
            return redirect(config('laraadmin.adminRoute')."/pedidos");
        }else{
            return view('la.dashboard');
        }
    }
}