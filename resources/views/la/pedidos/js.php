<?php
$role = DB::table('role_user')->select('role_id')->where('user_id',Auth::user()->id)->get();
if ($role[0]->role_id == '3') {
    echo '
    <script>
    $("[name=cliente_id]").prop("type", "hidden");
    $("[name=cliente_id]").val("'. Auth::user()->cliente_id .'");
    $("label[for=cliente_id]").hide();
    </script>
    ';
}else{
    echo '
    <script>
    $("[name=cliente_id]").remove();
    $("label[for=cliente_id]").append("<select class=form-control input-sm name=cliente_id></select>");
    $("[name=cliente_id]").append("';
    $clientes = DB::table('users')->select('name', 'cliente_id')->get();
    global $datos;
    foreach ($clientes as $cliente) {
        if (!empty($cliente->cliente_id)) {
            $datos = $datos . '<option value='.$cliente->cliente_id.'>'.$cliente->name.'</option>';
        }
    }
    echo $datos;
    echo '");
    </script>
    ';
}
?>