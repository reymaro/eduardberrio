@extends("la.layouts.app")

@section("contentheader_title", "Pedidos")
@section("contentheader_description", "Pedidos listing")
@section("section", "Pedidos")
@section("sub_section", "Listing")
@section("htmlheader_title", "Pedidos Listing")

@section("headerElems")
@la_access("Pedidos", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Pedido</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Pedidos", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Pedido</h4>
			</div>
			{!! Form::open(['action' => 'LA\PedidosController@store', 'id' => 'pedido-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                    @la_form($module)
					
					{{--
					@la_input($module, 'id')
					@la_input($module, 'numerofactura')
					@la_input($module, 'nombrealmacen')
					@la_input($module, 'nombrecliente')
					@la_input($module, 'totalneto')
					@la_input($module, 'estado')
					@la_input($module, 'fecha')
					@la_input($module, 'hora')
					@la_input($module, 'formapago')
					@la_input($module, 'direccion')
					@la_input($module, 'telefono')
					@la_input($module, 'numerofiscal')
					@la_input($module, 'fechacreacion')
					@la_input($module, 'observacion')
					@la_input($module, 'cliente_id')
					--}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script>
$("#example1").append(
    $('<tfoot/>').append( $("#example1 thead tr").clone() )
);
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        scrollX: true,
        dom: 'Bflrtip',
        buttons: [
            {
                extend: 'print',
                exportOptions: { 
                        orthogonal: 'export',
                        format: {
                        body: function( data, row, col, node ) {
                        var table = $("#example1").DataTable();
                            if (col == 5) {
                                return table
                                .cell( {row: row, column: col} )
                                .nodes()
                                .to$()
                                .find(':selected')
                                .text();
                            } else if (col == 1) {
                                return table
                                .cell( {row: row, column: col} )
                                .nodes()
                                .to$()
                                .text(); 
                            } else if (col == 12) {
                                return table
                                .cell( {row: row, column: col} )
                                .nodes()
                                .to$()
                                .text();
                            } else {
                                //data = data.replace(/<.*?>/g, '');
                                //return $.trim(data);
                                return data;
                            }
                        }
                    }
                },
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                        orthogonal: 'export',
                        format: {
                        body: function( data, row, col, node ) {
                        var table = $("#example1").DataTable();
                            if (col == 5) {
                                return table
                                .cell( {row: row, column: col} )
                                .nodes()
                                .to$()
                                .find(':selected')
                                .text();
                            } else if (col == 1) {
                                return table
                                .cell( {row: row, column: col} )
                                .nodes()
                                .to$()
                                .text(); 
                            } else if (col == 12) {
                                return table
                                .cell( {row: row, column: col} )
                                .nodes()
                                .to$()
                                .text();
                            } else {
                                //data = data.replace(/<.*?>/g, '');
                                //return $.trim(data);
                                return data;
                            }
                        }
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                exportOptions: { orthogonal: 'export' }
            }
        ],
        /*fixedHeader: {
            header: true,
            footer: false
        },*/
        ajax: "{{ url(config('laraadmin.adminRoute') . '/pedido_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
        columnDefs: [
            { orderable: false },
            //{ targets: [-1], visible: true},
            { targets: [5],
                render: function ( data, type, row, meta ) {
                    <?php
                    $role = DB::table('role_user')->select('role_id')->where('user_id',Auth::user()->id)->get();
                    if ($role[0]->role_id == '3') {
                    ?>
                    if(data == '0'){ data = 'No asignado'; }
                    if(data == '1'){ data = 'Asignado'; }
                    if(data == '2'){ data = 'En entrega'; }
                    if(data == '3'){ data = 'Entregado'; }
                    if(data == '4'){ data = 'Cancelado'; }
                    return data;
                    <?php
                    }else{
                    ?>
                    if(data == '0'){ data = '<select id="'+row[0]+'" name="estado"><option value="0" selected>No asignado</option><option value="1">Asignado</option><option value="2">En entrega</option><option value="3">Entregado</option><option value="4">Cancelado</option></select>'; }
                    if(data == '1'){ data = '<select id="'+row[0]+'" name="estado"><option value="0">No asignado</option><option value="1" selected>Asignado</option><option value="2">En entrega</option><option value="3">Entregado</option><option value="4">Cancelado</option></select>'; }
                    if(data == '2'){ data = '<select id="'+row[0]+'" name="estado"><option value="0">No asignado</option><option value="1">Asignado</option><option value="2" selected>En entrega</option><option value="3">Entregado</option><option value="4">Cancelado</option></select>'; }
                    if(data == '3'){ data = '<select id="'+row[0]+'" name="estado"><option value="0">No asignado</option><option value="1">Asignado</option><option value="2">En entrega</option><option value="3" selected>Entregado</option><option value="4">Cancelado</option></select>'; }
                    if(data == '4'){ data = '<select id="'+row[0]+'" name="estado"><option value="0">No asignado</option><option value="1">Asignado</option><option value="2">En entrega</option><option value="3">Entregado</option><option value="4" selected>Cancelado</option></select>'; }
                    return data;
                    <?php
                    }
                    ?>
                }
            }
        ],
		//columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
        initComplete: function (settings, json) {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()) )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                         column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
	});
	$("#pedido-add-form").validate({ });
});
</script>

<script>
$("#example1 tbody").on( "change", "select", function () {
    var pedido_id = $(this).attr('id');
    var estado = $(this).val();
    $.ajax({
        type: "GET",
        url : "{{ url(config('laraadmin.adminRoute') . '/pedido_cambia_estado/') }}/" + pedido_id + "/" + estado,
        //data : $(this).val(),
        success : function(data){
            console.log(data);
            location.reload();
        }
    },"html");
} );
</script>

<?php
$role = DB::table('role_user')->select('role_id')->where('user_id',Auth::user()->id)->get();
if ($role[0]->role_id == '3') {
    echo '
    <script>
    $("[name=cliente_id]").prop("type", "hidden");
    $("[name=cliente_id]").val("'. Auth::user()->cliente_id .'");
    $("label[for=cliente_id]").hide();
    var d = new Date,
    dformat = [d.getFullYear(),
               d.getMonth()+1,
               d.getDate()].join("-")+" "+
              [d.getHours(),
               d.getMinutes(),
               d.getSeconds()].join(":");
    $("[name=fechacreacion]").val(dformat);
    </script>
    ';
}else{
    echo '
    <script>
    $("[name=cliente_id]").remove();
    $("label[for=cliente_id]").append("<select style=margin-right:1000px; class=form-control input-sm name=cliente_id></select>");
    var d = new Date,
    dformat = [d.getFullYear(),
               d.getMonth()+1,
               d.getDate()].join("-")+" "+
              [d.getHours(),
               d.getMinutes(),
               d.getSeconds()].join(":");
    $("[name=fechacreacion]").val(dformat);
    $("[name=cliente_id]").append("';
    $clientes = DB::table('users')->select('name', 'cliente_id')->get();
    global $datos;
    foreach ($clientes as $cliente) {
        if (!empty($cliente->cliente_id)) {
            $datos = $datos . '<option value='.$cliente->cliente_id.'>'.$cliente->name.'</option>';
        }
    }
    echo $datos;
    echo '");
    $("[name=estado]").remove();
    $("label[for=estado]").append("<select style=margin-right:1000px; class=form-control input-sm name=estado><option value=0 selected>No asignado</option><option value=1>Asignado</option><option value=2>En entrega</option><option value=3>Entregado</option><option value=4>Cancelado</option></select>");
    </script>
    ';
}
?>
@endpush
