@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/pedidos') }}">Pedido</a> :
@endsection
@section("contentheader_description", $pedido->$view_col)
@section("section", "Pedidos")
@section("section_url", url(config('laraadmin.adminRoute') . '/pedidos'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Pedidos Edit : ".$pedido->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($pedido, ['route' => [config('laraadmin.adminRoute') . '.pedidos.update', $pedido->id ], 'method'=>'PUT', 'id' => 'pedido-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'id')
					@la_input($module, 'numerofactura')
					@la_input($module, 'nombrealmacen')
					@la_input($module, 'nombrecliente')
					@la_input($module, 'totalneto')
					@la_input($module, 'estado')
					@la_input($module, 'fecha')
					@la_input($module, 'hora')
					@la_input($module, 'formapago')
					@la_input($module, 'direccion')
					@la_input($module, 'telefono')
					@la_input($module, 'numerofiscal')
					@la_input($module, 'fechacreacion')
					@la_input($module, 'observacion')
					@la_input($module, 'cliente_id')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/pedidos') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#pedido-edit-form").validate({
		
	});
});
</script>
@endpush
